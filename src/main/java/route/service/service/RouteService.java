package route.service.service;

import org.springframework.data.util.Pair;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import route.service.model.Leg;
import route.service.model.Route;
import route.service.model.Waypoint;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class RouteService {

    /**
     *
     * @param route route
     * @param maxTimePerDay time in s
     */
    public void partition(Route route, int maxTimePerDay) {
        double currTime = route.getLegs().get(0).getDuration();
        for (Leg leg : route.getLegs()) {
            if (currTime >= maxTimePerDay) {
                break;
            } else if (currTime + leg.getDuration() + leg.getTo().getEventDuration() >= maxTimePerDay) {
                break;
            } else {
                currTime += leg.getDuration() + leg.getTo().getEventDuration();
            }
        }
    }

    public Route calculateRoute(List<String[]> durationMatrix, List<Waypoint> waypoints) {
        Route route = new Route();
        int index = 0;
        for (int i = 0; i < waypoints.size() - 1; i++) {
            Pair<Double, Integer> pair = getMin(Arrays.stream(durationMatrix.get(index)).map(Double::parseDouble).collect(Collectors.toList()));
            route.getLegs().add(new Leg(waypoints.get(index), waypoints.get(pair.getSecond()), pair.getFirst()));
            index = pair.getSecond();
        }
        return route;
    }

    // 1ое значение - длительность, 2ое значение - позиция точки в матрице
    private Pair<Double, Integer> getMin(List<Double> durationsBetweenWaypoints) {
        int min = Integer.MAX_VALUE;
        Pair<Double, Integer> pair = null;
        for (int i = 0; i < durationsBetweenWaypoints.size(); i++) {
            if (durationsBetweenWaypoints.get(i) < min && durationsBetweenWaypoints.get(i) != 0) {
                pair = Pair.of(durationsBetweenWaypoints.get(i), i);
            }
        }
        return pair;
    }

    public List<Waypoint> getWaypoints(String coords, List<Double> eventDurations) {
        List<Waypoint> waypoints = Arrays.stream(coords.split(";"))
                .map(x -> x.split(","))
                .map(x -> new Waypoint(Double.parseDouble(x[0]), Double.parseDouble(x[1])))
                .collect(Collectors.toList());

        for (int i = 0; i < eventDurations.size(); i++) {
            waypoints.get(i).setEventDuration(eventDurations.get(i));
        }

        return waypoints;
    }

    public String doGetRequestToOSRM(String url, String coords) {
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity = new HttpEntity<>(headers);
        ResponseEntity<String> responseEntity;
        responseEntity = restTemplate.exchange(String.format(url, coords), HttpMethod.GET, entity, String.class);
        return responseEntity.getBody();
    }

    public List<Double> parseEventsDurations(String eventDurations) {
        return Arrays.stream(eventDurations.split(","))
                .map(Double::valueOf)
                .collect(Collectors.toList());
    }
}
