package route.service.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.springframework.data.util.Pair;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import route.service.model.Leg;
import route.service.model.Route;
import route.service.model.Waypoint;
import route.service.service.RouteService;

import java.util.*;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/routes")
public class RouteController {

    private final String OSRM_URL_TEMP = "http://router.project-osrm.org/table/v1/driving/%s";
    private final String OSRM_URL_CAR = "http://osrmcar:5000/table/v1/driving/%s";
    private final String OSRM_URL_FOOT = "http://osrmfoot:5000/table/v1/foot/%s";

    private final RouteService routeService;

    public RouteController(RouteService routeService) {
        this.routeService = routeService;
    }

    @GetMapping
    public ResponseEntity<List<Leg>> getRoute(@RequestParam("type") String type, @RequestParam("waypoints") String coords) throws JsonProcessingException {
        //todo all
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity = new HttpEntity<>(headers);
        ResponseEntity<String> responseEntity;
        if (type.equals("car")) {
             responseEntity = restTemplate.exchange(String.format(OSRM_URL_CAR, coords), HttpMethod.GET, entity, String.class);
        } else if (type.equals("foot")) /*if (type.equals("foot"))*/ {
             responseEntity = restTemplate.exchange(String.format(OSRM_URL_FOOT, coords), HttpMethod.GET, entity, String.class);
        } else {
            responseEntity = restTemplate.exchange(String.format(OSRM_URL_TEMP, coords), HttpMethod.GET, entity, String.class);
        }
        String response = responseEntity.getBody();

        List<Leg> route = new ArrayList<>();
        List<Waypoint> waypoints = Arrays.stream(coords.split(";"))
                .map(x -> x.split(","))
                .map(x -> new Waypoint(Double.parseDouble(x[0]), Double.parseDouble(x[1])))
                .collect(Collectors.toList());
        List<Double> durations = new ArrayList<>();

        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.enable(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY);
        ObjectNode root = new ObjectMapper().readValue(response, ObjectNode.class);

        JsonNode durationsNode = root.get("durations");

        List<String[]> durationMatrix = parse(durationsNode.toString());
        Set<Waypoint> passed = new LinkedHashSet<>();

        int index = 0;
        passed.add(waypoints.get(0));
        for (int i = 0; i < waypoints.size() - 1; i++) {
            Pair<Double, Integer> pair = getMin(Arrays.stream(durationMatrix.get(index)).map(Double::parseDouble).collect(Collectors.toList()), waypoints, passed);
            route.add(new Leg(waypoints.get(index), waypoints.get(pair.getSecond()), pair.getFirst()));
            durations.add(pair.getFirst());
            index = pair.getSecond();
        }
        return new ResponseEntity<>(route, HttpStatus.OK);
    }

    // 1ое значение - длительность, 2ое значение - позиция точки в матрице
    private Pair<Double, Integer> getMin(List<Double> durationsBetweenWaypoints, List<Waypoint> waypoints, Set<Waypoint> passed) {
        double min = Double.MAX_VALUE;
        Pair<Double, Integer> pair = null;
        for (int i = 0; i < durationsBetweenWaypoints.size(); i++) {
            if (durationsBetweenWaypoints.get(i) < min && durationsBetweenWaypoints.get(i) != 0
                    && !passed.contains(waypoints.get(i))) {
                min = durationsBetweenWaypoints.get(i);
                pair = Pair.of(durationsBetweenWaypoints.get(i), i);
            }
        }
        assert pair != null;
        passed.add(waypoints.get(pair.getSecond()));
        return pair;
    }


    @GetMapping("/test")
    public ResponseEntity<Route> testGetRoute(@RequestParam("type") String type,
                                              @RequestParam("waypoints") String coords,
                                              @RequestParam("durations") String eventDurationsParam) throws JsonProcessingException {

        String response;
        if (type.equals("car")) {
            response = routeService.doGetRequestToOSRM(OSRM_URL_CAR, coords);
        } else {
            response = routeService.doGetRequestToOSRM(OSRM_URL_FOOT, coords);
        }

        Route route = new Route();
        List<Leg> legs = new ArrayList<>();
        List<Waypoint> waypoints = routeService.getWaypoints(coords, routeService.parseEventsDurations(eventDurationsParam));
//        List<Double> eventDurations = routeService.parseEventsDurations(eventDurationsParam);

        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.enable(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY);
        ObjectNode root = new ObjectMapper().readValue(response, ObjectNode.class);

        JsonNode durationsNode = root.get("durations");
        List<String[]> durationMatrix = parse(durationsNode.toString());

        int index = 0;
        for (int i = 0; i < waypoints.size() - 1; i++) {
//            Pair<Double, Integer> pair = getMin(Arrays.stream(durationMatrix.get(index)).map(Double::parseDouble).collect(Collectors.toList()), waypoints, );
//            legs.add(new Leg(waypoints.get(index), waypoints.get(pair.getSecond()), pair.getFirst()));
//            index = pair.getSecond();
        }
        return new ResponseEntity<>(route, HttpStatus.OK);
    }




    //todo переделать
    private List<String[]> parse(String src) {
        src = src.substring(1, src.length() - 1).replace("[", "").replace("],", "]");
        List<String[]> durations = new ArrayList<>();
        for (String duration : src.split("]")) {
            durations.add(duration.split(","));
        }
        return durations;
    }

}
