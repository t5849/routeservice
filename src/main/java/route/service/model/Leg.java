package route.service.model;


public class Leg {
    private Waypoint from;
    private Waypoint to;
    private Double duration;

    public Leg(Waypoint from, Waypoint to, Double duration) {
        this.from = from;
        this.to = to;
        this.duration = duration;
    }

    public Waypoint getFrom() {
        return from;
    }

    public void setFrom(Waypoint from) {
        this.from = from;
    }

    public Waypoint getTo() {
        return to;
    }

    public void setTo(Waypoint to) {
        this.to = to;
    }

    public Double getDuration() {
        return duration;
    }

    public void setDuration(Double duration) {
        this.duration = duration;
    }
}
