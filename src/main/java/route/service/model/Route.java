package route.service.model;

import java.util.ArrayList;
import java.util.List;

public class Route {
    private List<Leg> legs;
    private Double duration;

    public Route(List<Leg> legs) {
        this.legs = legs;
    }

    public Route() {
        legs = new ArrayList<>();
    }

    public List<Leg> getLegs() {
        return legs;
    }

    public void setLegs(List<Leg> legs) {
        this.legs = legs;
    }

    public Double getDuration() {
        return duration;
    }

    public void setDuration(Double duration) {
        this.duration = duration;
    }
}
