FROM openjdk:11-jre-slim AS run
COPY target/routeService-latest.jar /usr/local/lib/app.jar
ENTRYPOINT ["java","-jar","/usr/local/lib/app.jar"]
